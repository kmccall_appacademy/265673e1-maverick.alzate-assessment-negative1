# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_nums = []
  index = 0
  while index < nums.length - 1
    if nums[index + 1] != nums[index] + 1
      range = *(nums[index] + 1...nums[index + 1])
      missing_nums += range
    end
    index += 1
  end

  missing_nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  decimal = 0

  bi_exp = binary.to_s.length - 1
  binary.to_s.chars.each do |digit|
    decimal += digit.to_i * 2**bi_exp
    bi_exp -= 1
  end

  decimal
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    selected = Hash.new()
    self.each do |key, value|
      selected[key] = value if prc.call(key, value) == true
    end
    selected
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged_hash = {}
    # iterate through keys both hashes share
    self.each do |key, value|
      if block_given?
        merged_hash[key] = prc.call(key, value, hash[key])
      elsif hash.key?(key)
        merged_hash[key] = hash[key]
      else
        merged_hash[key] = value
      end
    end

    # add exclusive other keys to merged_hash
    new_keys = hash.reject { |key, value| self.key?(key) }
    new_keys.each { |key, value| merged_hash[key] = value }
    # return merged_hash
    merged_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  series = [2, 1]
  index = 2
  while index < n.abs + 5
    series[index] = series[index - 1] + series[index - 2]
    index += 1
  end

  if n >= 0
    return series[n.abs]
  elsif n % 2 == 0
    return series[n.abs]
  else
    return series[n.abs] * -1
  end

end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  # get all substrings from string
  substrings = []
  letters = string.chars
  letters.each_index do |index1|
    ((index1 + 1)...letters.length).each do |index2|
      substrings << letters[index1] + letters[index1 + 1..index2].join
    end
  end

  # get palindromes
  palindromes = substrings.select { |s| palindrome?(s) }
  palindromes.sort_by!(&:length)
  # if there is palindrome longer than two letters, return it
  return false if palindromes.empty?
  palindromes[-1].length > 2 ? palindromes[-1].length : false
end

def palindrome?(str)
  true if str == str.reverse
end
